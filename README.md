# GitLab CI image for Android

GitLab provides a template CI config for building Android apps, described in detail
at https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/.
However, the base image does not include the Android SDK tools, resulting in a fairly
complex CI configuration.

This image integrates all the required tools but is otherwise identical to the one
used by the Android CI template. It will work in exactly the same way, except that
you can greatly simplify your CI config now, and your pipelines will take about
30 seconds less.

## Sample `.gitlab-cy-yml`

```
image: registry.gitlab.com/mvglasow/android-gitlab-ci:master

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
  - mkdir -p $GRADLE_USER_HOME
  - chmod +x ./gradlew

cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches

assembleDebug:
  stage: build
  script:
    - ./gradlew assembleDebug
  artifacts:
    paths:
      - build/outputs/
```