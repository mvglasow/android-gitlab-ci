#
# GitLab CI for Android
#
# Based on the GitLab CI template for Android, described in detail at
# https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/, by Jason Lenny
#

FROM openjdk:8-jdk
MAINTAINER mvglasow <michael@vonglasow.com>

# Prepare System
ENV DEBIAN_FRONTEND noninteractive
ENV ANDROID_COMPILE_SDK "28"
ENV ANDROID_BUILD_TOOLS "28.0.2"
ENV ANDROID_SDK_TOOLS "4333796"
ENV ANDROID_HOME "/opt/android-sdk-linux"

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-$ANDROID_SDK_TOOLS.zip
RUN unzip -d $ANDROID_HOME android-sdk.zip

ENV PATH "${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools"

RUN echo y | sdkmanager "platforms;android-$ANDROID_COMPILE_SDK" >/dev/null
RUN echo y | sdkmanager "platform-tools" >/dev/null
RUN echo y | sdkmanager "build-tools;$ANDROID_BUILD_TOOLS" >/dev/null

RUN yes | sdkmanager --licenses
